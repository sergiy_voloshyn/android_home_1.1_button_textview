package com.xyz.home_1;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/*
1. Создать приложение с одним экраном. Добавить на экран текстовое поле (TextView) и две кнопки (Button).
 Первая кнопка должна печатать в текстовом поле фразу "привет мир!". Вторая кнопка должна очищать текстовое поле.
 */

public class MainActivity extends AppCompatActivity {

    TextView helloWorld;
    Button actionSet;
    Button actionClear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);


        helloWorld = (TextView) findViewById(R.id.hello_world);
        actionSet = (Button) findViewById(R.id.actionSet);
        actionClear = (Button) findViewById(R.id.actionClear);


        actionSet.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        helloWorld.setText(R.string.hello_sourceit);
                        helloWorld.setTextColor(Color.BLUE);
                        helloWorld.setAllCaps(true);
                    }
                }
        );


        actionClear.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        helloWorld.setText("");
                    }
                }
        );

    }
}
